package plugins.ylemontag.matlabimporter;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import plugins.ylemontag.matlabio.gui.ComplexModeComponent;
import plugins.ylemontag.matlabio.gui.DimensionMappingComponent;
import plugins.ylemontag.matlabio.gui.FileChooserComponent;

public class MainPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private FileChooserComponent _fileChooserComponent;
	private DimensionMappingComponent _dimensionMappingComponent;
	private VariableListComponent _variableListComponent;
	private JButton _actionButton;
	private JCheckBox _doPrefixComponent;
	private ComplexModeComponent _complexModeComponent;
	private JButton _refreshButton;
	
	/**
	 * Matlab file chooser component
	 */
	public FileChooserComponent getFileComponent() {
		return _fileChooserComponent;
	}
	
	/**
	 * Dimension mapping component
	 */
	public DimensionMappingComponent getDimensionMappingComponent() {
		return _dimensionMappingComponent;
	}
	
	/**
	 * Complex mode component
	 */
	public ComplexModeComponent getComplexModeComponent() {
		return _complexModeComponent;
	}
	
	/**
	 * Variable list component
	 */
	public VariableListComponent getVariableComponent() {
		return _variableListComponent;
	}
	
	/**
	 * Prefix check-box
	 */
	public JCheckBox getDoPrefixComponent() {
		return _doPrefixComponent;
	}
	
	/**
	 * Refresh button
	 */
	public JButton getRefreshButton() {
		return _refreshButton;
	}
	
	/**
	 * Action button
	 */
	public JButton getActionButton() {
		return _actionButton;
	}

	/**
	 * Create the panel
	 */
	public MainPanel() {
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel_8 = new JPanel();
		add(panel_8, BorderLayout.SOUTH);
		panel_8.setLayout(new BorderLayout(0, 0));
		
		Component verticalStrut_1 = Box.createVerticalStrut(5);
		panel_8.add(verticalStrut_1, BorderLayout.NORTH);
		
		JPanel panel_9 = new JPanel();
		panel_8.add(panel_9, BorderLayout.EAST);
		panel_9.setLayout(new BoxLayout(panel_9, BoxLayout.X_AXIS));
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel_9.add(horizontalStrut_1);
		
		_refreshButton = new JButton("Refresh");
		_refreshButton.setToolTipText("Refresh the list of variables");
		panel_9.add(_refreshButton);
		
		Component horizontalStrut_9 = Box.createHorizontalStrut(5);
		panel_9.add(horizontalStrut_9);
		
		_actionButton = new JButton("Import");
		_actionButton.setToolTipText("Import the selected objects");
		panel_9.add(_actionButton);
		
		Component horizontalGlue_2 = Box.createHorizontalGlue();
		panel_8.add(horizontalGlue_2, BorderLayout.CENTER);
		
		_doPrefixComponent = new JCheckBox("Prefix with filename");
		_doPrefixComponent.setToolTipText("The name of the imported sequences will be prefixed with the name of the Matlab .mat file");
		panel_8.add(_doPrefixComponent, BorderLayout.WEST);
		
		_variableListComponent = new VariableListComponent();
		
		JScrollPane scrollPane = new JScrollPane(_variableListComponent);
		add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		Component verticalStrut = Box.createVerticalStrut(5);
		panel_1.add(verticalStrut, BorderLayout.SOUTH);
		
		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4, BorderLayout.CENTER);
		panel_4.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_4.setLayout(new BorderLayout(0, 0));
		
		Component verticalStrut_5 = Box.createVerticalStrut(5);
		panel_4.add(verticalStrut_5, BorderLayout.NORTH);
		
		Component verticalStrut_6 = Box.createVerticalStrut(5);
		panel_4.add(verticalStrut_6, BorderLayout.SOUTH);
		
		Component horizontalStrut_4 = Box.createHorizontalStrut(5);
		panel_4.add(horizontalStrut_4, BorderLayout.EAST);
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5, BorderLayout.CENTER);
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.Y_AXIS));
		
		JPanel panel_7 = new JPanel();
		panel_5.add(panel_7);
		panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.X_AXIS));
		
		JLabel lblNewLabel = new JLabel("Source file");
		panel_7.add(lblNewLabel);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		panel_7.add(horizontalGlue);
		
		Component verticalStrut_2 = Box.createVerticalStrut(5);
		panel_5.add(verticalStrut_2);
		
		JPanel panel_6 = new JPanel();
		panel_5.add(panel_6);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Matlab object files (*.mat)", "mat");
		_fileChooserComponent = new FileChooserComponent(FileChooserComponent.Mode.OPEN_DIALOG, filter);
		panel_6.add(_fileChooserComponent);
		
		Component horizontalStrut_5 = Box.createHorizontalStrut(5);
		panel_4.add(horizontalStrut_5, BorderLayout.WEST);
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.EAST);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		panel_2.add(horizontalStrut);
		
		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3);
		panel_3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_3.setLayout(new BorderLayout(0, 0));
		
		Component verticalStrut_3 = Box.createVerticalStrut(5);
		panel_3.add(verticalStrut_3, BorderLayout.NORTH);
		
		Component verticalStrut_4 = Box.createVerticalStrut(5);
		panel_3.add(verticalStrut_4, BorderLayout.SOUTH);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(5);
		panel_3.add(horizontalStrut_2, BorderLayout.WEST);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(5);
		panel_3.add(horizontalStrut_3, BorderLayout.EAST);
		
		JPanel panel_10 = new JPanel();
		panel_3.add(panel_10, BorderLayout.CENTER);
		panel_10.setLayout(new BoxLayout(panel_10, BoxLayout.Y_AXIS));
		
		JPanel panel_12 = new JPanel();
		panel_10.add(panel_12);
		panel_12.setLayout(new BoxLayout(panel_12, BoxLayout.X_AXIS));
		
		JLabel lblNewLabel_1 = new JLabel("Dimension mapping");
		panel_12.add(lblNewLabel_1);
		
		Component horizontalGlue_1 = Box.createHorizontalGlue();
		panel_12.add(horizontalGlue_1);
		
		Component verticalStrut_7 = Box.createVerticalStrut(5);
		panel_10.add(verticalStrut_7);
		
		JPanel panel_11 = new JPanel();
		panel_10.add(panel_11);
		panel_11.setLayout(new BorderLayout(0, 0));
		
		_dimensionMappingComponent = new DimensionMappingComponent();
		_dimensionMappingComponent.setToolTipText("Describe how a multi-dimension Matlab array should be interpreted as a sequence");
		panel_11.add(_dimensionMappingComponent, BorderLayout.CENTER);
		
		Component horizontalStrut_6 = Box.createHorizontalStrut(5);
		panel_2.add(horizontalStrut_6);
		
		JPanel panel_13 = new JPanel();
		panel_13.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_2.add(panel_13);
		panel_13.setLayout(new BorderLayout(0, 0));
		
		Component horizontalStrut_7 = Box.createHorizontalStrut(5);
		panel_13.add(horizontalStrut_7, BorderLayout.WEST);
		
		Component horizontalStrut_8 = Box.createHorizontalStrut(5);
		panel_13.add(horizontalStrut_8, BorderLayout.EAST);
		
		Component verticalStrut_8 = Box.createVerticalStrut(5);
		panel_13.add(verticalStrut_8, BorderLayout.NORTH);
		
		Component verticalStrut_9 = Box.createVerticalStrut(5);
		panel_13.add(verticalStrut_9, BorderLayout.SOUTH);
		
		JPanel panel_14 = new JPanel();
		panel_13.add(panel_14, BorderLayout.CENTER);
		panel_14.setLayout(new BoxLayout(panel_14, BoxLayout.Y_AXIS));
		
		JPanel panel_15 = new JPanel();
		panel_14.add(panel_15);
		panel_15.setLayout(new BoxLayout(panel_15, BoxLayout.X_AXIS));
		
		JLabel lblNewLabel_2 = new JLabel("Complex variables");
		panel_15.add(lblNewLabel_2);
		
		Component horizontalGlue_3 = Box.createHorizontalGlue();
		panel_15.add(horizontalGlue_3);
		
		Component verticalStrut_10 = Box.createVerticalStrut(5);
		panel_14.add(verticalStrut_10);
		
		_complexModeComponent = new ComplexModeComponent();
		_complexModeComponent.setToolTipText("Describe how complex-valued Matlab arrays should be imported: only the real part, only the imaginary part, or both. In the latter, the real data will be saved in even channels, and imaginary data in odd channels.");
		panel_14.add(_complexModeComponent);

	}

}
