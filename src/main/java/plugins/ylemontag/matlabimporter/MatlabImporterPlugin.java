package plugins.ylemontag.matlabimporter;

import icy.gui.dialog.MessageDialog;
import icy.gui.frame.IcyFrame;
import icy.gui.frame.progress.ProgressFrame;
import icy.plugin.abstract_.PluginActionable;
import icy.preferences.XMLPreferences;
import icy.sequence.Sequence;
import icy.system.thread.ThreadUtil;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JCheckBox;

import plugins.ylemontag.matlabio.ComplexMode;
import plugins.ylemontag.matlabio.DimensionMapping;
import plugins.ylemontag.matlabio.MatlabImporter;
import plugins.ylemontag.matlabio.gui.ComplexModeComponent;
import plugins.ylemontag.matlabio.gui.DimensionMappingComponent;
import plugins.ylemontag.matlabio.gui.DimensionMappingComponent.DimensionMappingChangedListener;
import plugins.ylemontag.matlabio.gui.FileChooserComponent;
import plugins.ylemontag.matlabio.gui.FileChooserComponent.FileChangedListener;
import plugins.ylemontag.matlabio.gui.MatlabProgressFrame;
import plugins.ylemontag.matlabio.lib.Controller;
import plugins.ylemontag.matlabio.lib.MLMeta;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Plugin to import .mat file as sequences
 */
public class MatlabImporterPlugin extends PluginActionable
{	
	private FileChooserComponent _fileChooserComponent;
	private DimensionMappingComponent _dimensionMappingComponent;
	private ComplexModeComponent _complexModeComponent;
	private JCheckBox _doPrefixComponent;
	private VariableListComponent _variableListComponent;
	private JButton _refreshButton;
	private JButton _actionButton;
	private File _file;
	private MatlabImporter _importer;
	
	@Override
	public void run()
	{	
		// GUI
		IcyFrame frame = new IcyFrame("Matlab importer", true, true, true, true);
		frame.setSize        (500, 400);
		frame.setSizeExternal(500, 400);
		MainPanel mainPanel = new MainPanel();
		frame.add(mainPanel);
		addIcyFrame(frame);
		frame.center();
		frame.setVisible(true);
		frame.requestFocus();
		_fileChooserComponent = mainPanel.getFileComponent();
		_dimensionMappingComponent = mainPanel.getDimensionMappingComponent();
		_complexModeComponent = mainPanel.getComplexModeComponent();
		_doPrefixComponent = mainPanel.getDoPrefixComponent();
		_variableListComponent = mainPanel.getVariableComponent();
		_refreshButton = mainPanel.getRefreshButton();
		_actionButton = mainPanel.getActionButton();
		retrievePersistentPrefix();
		retrievePersistentDimensionMapping();
		retrievePersistentComplexMode();
		retrievePersistentDirectory();
		
		// Listeners
		_fileChooserComponent.addFileChangedListener(new FileChangedListener()
		{	
			@Override
			public void actionPerformed(File newFile) {
				refreshVariableList(true, null);
				updatePersistentDirectory();
			}
		});
		_dimensionMappingComponent.addDimensionMappingChangedListener(new DimensionMappingChangedListener()
		{	
			@Override
			public void actionPerformed() {
				updatePersistentDimensionMapping();
			}
		});
		_complexModeComponent.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				updatePersistentComplexMode();
			}
		});
		_doPrefixComponent.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				updatePersistentPrefix();
			}
		});
		_refreshButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshVariableList(false, _variableListComponent.getSelectedVariables());
			}
		});
		_actionButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				executeImport();
			}
		});
		_variableListComponent.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getButton()==1 && e.getClickCount()==2) {
					executeImport();
				}
			}
		});
	}
	
	/**
	 * Refresh the component that display the list of importable variables in
	 * the currently selected .mat file
	 */
	private void refreshVariableList(boolean refreshFile, List<String> selectedVariables)
	{
		// Refresh the file from the dedicated component
		if(refreshFile) {
			_file = _fileChooserComponent.getSelectedFile();
		}
		
		// If no file is selected
		_variableListComponent.clearRows();
		_importer = null;
		if(_file==null) {
			return;
		}
		
		// Try to rebuild the importer
		try {
			_importer = new MatlabImporter(_file);
		}
		catch(IOException err) {
			String message = "Cannot read the Matlab file " + _file.getPath();
			MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
			return;
		}
		
		// Refresh the list
		Set<String> names = _importer.getImportableSequences();
		for(String name : names) {
			MLMeta meta = _importer.getMeta(name);
			_variableListComponent.addRow(meta);
		}
		
		// Enforce the selection
		if(selectedVariables!=null) {
			_variableListComponent.setSelectedVariables(selectedVariables);
		}
	}
	
	/**
	 * Retrieve the current prefix
	 */
	private String getCurrentPrefix()
	{
		String retVal = "";
		if(_doPrefixComponent.isSelected() && _file!=null) {
			retVal = _file.getName();
			int dotPosition = retVal.lastIndexOf('.');
			if(dotPosition>=0) {
				retVal = retVal.substring(0, dotPosition);
			}
			retVal += "/";
		}
		return retVal;
	}
	
	/**
	 * Do the importing job
	 */
	private void executeImport()
	{
		final List<String> names = _variableListComponent.getSelectedVariables();
		_variableListComponent.clearSelection();
		for(final String name : names) {
			final DimensionMapping mapping     = _dimensionMappingComponent.getDimensionMapping();
			final ComplexMode      complexMode = _complexModeComponent.getComplexMode();
			final String           prefix      = getCurrentPrefix();
			final Controller    controller     = new Controller();
			final ProgressFrame progressFrame  = new MatlabProgressFrame("Importing sequence " + name, controller);
			ThreadUtil.bgRun(new Runnable()
			{	
				@Override
				public void run() {
					try {
						Sequence seq = _importer.getSequence(name, mapping, complexMode, prefix+name, controller);
						addSequence(seq);
					}
					catch(final IOException err) {
						ThreadUtil.invokeLater(new Runnable()
						{	
							@Override
							public void run() {
								System.err.println(err);
								String message = "Cannot import the variable '" + name + "'";
								MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
							}
						});
					}
					catch(Controller.CanceledByUser err) {}
					finally {
						ThreadUtil.invokeLater(new Runnable()
						{	
							@Override
							public void run() {
								progressFrame.close();
							}
						});
					}
				}
			});
		}
	}
	
	/**
	 * Retrieve the persistent dimension mapping parameters
	 */
	private void retrievePersistentDimensionMapping()
	{
		XMLPreferences prefs = getPreferences("DimensionMapping");
		DimensionMapping mapping = _dimensionMappingComponent.getDimensionMapping();
		int dimX = mapping.getDimensionX();
		int dimY = mapping.getDimensionY();
		int dimZ = mapping.getDimensionZ();
		int dimT = mapping.getDimensionT();
		int dimC = mapping.getDimensionC();
		dimX = prefs.getInt("DimX", dimX);
		dimY = prefs.getInt("DimY", dimY);
		dimZ = prefs.getInt("DimZ", dimZ);
		dimT = prefs.getInt("DimT", dimT);
		dimC = prefs.getInt("DimC", dimC);
		_dimensionMappingComponent.setDimensions(dimX, dimY, dimZ, dimT, dimC);
	}
	
	/**
	 * Update the persistent dimension mapping parameters
	 */
	private void updatePersistentDimensionMapping()
	{
		XMLPreferences prefs = getPreferences("DimensionMapping");
		DimensionMapping mapping = _dimensionMappingComponent.getDimensionMapping();
		prefs.putInt("DimX", mapping.getDimensionX());
		prefs.putInt("DimY", mapping.getDimensionY());
		prefs.putInt("DimZ", mapping.getDimensionZ());
		prefs.putInt("DimT", mapping.getDimensionT());
		prefs.putInt("DimC", mapping.getDimensionC());
	}
	
	/**
	 * Retrieve the persistent directory
	 */
	private void retrievePersistentDirectory()
	{
		XMLPreferences prefs = getPreferences("Directory");
		String directoryPath = _fileChooserComponent.getCurrentDirectory().getAbsolutePath();
		directoryPath = prefs.get("Path", directoryPath);
		_fileChooserComponent.setCurrentDirectory(new File(directoryPath));
	}
	
	/**
	 * Update the persistent directory
	 */
	private void updatePersistentDirectory()
	{
		XMLPreferences prefs = getPreferences("Directory");
		prefs.put("Path", _fileChooserComponent.getCurrentDirectory().getAbsolutePath());
	}
	
	/**
	 * Retrieve the persistent complex mode parameter
	 */
	private void retrievePersistentComplexMode()
	{
		XMLPreferences prefs = getPreferences("ComplexMode");
		ComplexMode complexMode = _complexModeComponent.getComplexMode();
		complexMode = ComplexMode.fromDescription(prefs.get("Name", complexMode.getDescription()));
		if(complexMode!=null) {
			_complexModeComponent.setComplexMode(complexMode);
		}
	}
	
	/**
	 * Update the persistent complex mode
	 */
	private void updatePersistentComplexMode()
	{
		XMLPreferences prefs = getPreferences("ComplexMode");
		prefs.put("Name", _complexModeComponent.getComplexMode().getDescription());
	}
	
	/**
	 * Retrieve the persistent prefix parameter
	 */
	private void retrievePersistentPrefix()
	{
		XMLPreferences prefs = getPreferences("Prefix");
		boolean doPrefix = prefs.getBoolean("UseFilename", _doPrefixComponent.isSelected());
		_doPrefixComponent.setSelected(doPrefix);
	}
	
	/**
	 * Update the persistent prefix parameter
	 */
	private void updatePersistentPrefix()
	{
		XMLPreferences prefs = getPreferences("Prefix");
		prefs.putBoolean("UseFilename", _doPrefixComponent.isSelected());
	}
}
