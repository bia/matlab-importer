package plugins.ylemontag.matlabimporter;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import plugins.ylemontag.matlabio.lib.MLMeta;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component used to display a list of Matlab objects contained in a .mat file
 */
public class VariableListComponent extends JTable
{
	private static final long serialVersionUID = 1L;
	
	private DefaultTableModel _model;
	
	/**
	 * Constructor
	 */
	public VariableListComponent()
	{
		super();
		_model = new Model();
		setModel(_model);
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(_model);
		setRowSorter(sorter);
	}
	
	/**
	 * Return the variable name corresponding to a given row. The row index is
	 * relative to the JTable (not to the model)
	 */
	public String getVariableAt(int rowIndex)
	{
		return (String)getValueAt(rowIndex, 0);
	}
	
	/**
	 * Return the list of selected variables
	 */
	public List<String> getSelectedVariables()
	{
		int[] selectedRows = getSelectedRows();
		List<String> retVal = new LinkedList<String>();
		for(int selectedRow : selectedRows) {
			retVal.add(getVariableAt(selectedRow));
		}
		return retVal;
	}
	
	/**
	 * Set the list of selected variables
	 */
	public void setSelectedVariables(List<String> selectedVariables)
	{
		clearSelection();
		for(int r=0; r<getRowCount(); ++r) {
			if(selectedVariables.contains(getVariableAt(r))) {
				getSelectionModel().addSelectionInterval(r, r);
			}
		}
	}
	
	/**
	 * Add a row the table
	 */
	public void addRow(MLMeta meta)
	{
		String  name = meta.getName();
		String  dims = meta.getDimensionsAsString();
		Boolean cplx = meta.getIsComplex();
		String  type = meta.getType().toString();
		_model.addRow(new Object[] {name, type, cplx, dims});
	}
	
	/**
	 * Remove all the rows
	 */
	public void clearRows()
	{
		_model.setRowCount(0);
	}
	
	/**
	 * Model used in the component
	 */
	private static class Model extends DefaultTableModel
	{
		private static final long serialVersionUID = 1L;
		
		/**
		 * Constructor
		 */
		public Model()
		{
			super();
			addColumn("Variable"  );
			addColumn("Type"      );
			addColumn("Complex"   );
			addColumn("Dimensions");
		}
		
		@Override
		public boolean isCellEditable(int row, int column)
		{
			return false;
		}
		
		@Override
		public int getColumnCount()
		{
			return 4;
		}
		
		@Override
		public Class<?> getColumnClass(int columnIndex)
		{
			return columnIndex==2 ? Boolean.class : String.class;
		}
	}
}
